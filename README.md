# Free Camera and Camera Changer

The free camera and camera changer are two simple tools made on the Unity engine. These tools can be useful when testing the game, when we need to quickly move from one place to another. You can also use them as mechanic in the game. It all depends on you.

## Free Camera

Free camera allows us to move everywhere we want. It works just like noclip.
Free camera is very easy to use. It's enough to use a ready prefab or create a new camera and use the script "Free Camera".

**Script setting**
The script is very easy to use. Just set the speed of the camera and mouse. In addition, you can set TextMeshPro which will show the camera and mouse speed.

Note, that the speed shown is not shown in real time. However, you can use the additional "Settings" script, which is a very simple pause script. More on this below.

**Keyboard controls**

 - Use **W A S D** to move camera
 - Use **Q** and **E** to move up and down.
 - Hold down **Shift** to move faster.
 - While holding  **RMB** 
	 * Scroll up or down to speed up or slow down the camera movement
	 * Move the mouse to look around
 

## Camera Changer

Camera changer is a very simple tool that allows you to add several cameras to the scene and move between them during the game.

**Please note that the camera changer can be associated with a free camera.** 
Of course, you don't have to use the full functionality of camera changer, but if you want to take advantage of changes in the camera position in real time, you have to insert a free camera into the camera changer as one of the available cameras. This will allow you to move to the selected position and set a static camera in it.
It is equally possible to use all free cameras and switch between them at your own discretion or use all static cameras that have been set up before.

**Script setting**
Setting this script is also very simple. Insert in the camera array all cameras that you want to change between.
You can also add TextMeshPro and Animator which will display the currently used camera and whether the position of the selected camera has been saved.

**Keyboard controls**

- To change cameras use the **0 to 9 numeric keys**
 - Using the free camera, select the position where you want to save the static camera and using **0 to 9 *KeyPad* numeric keys** save it 
 - Use **F**  to select camera 0 (Faster method)
 
 *If you use fewer cameras then remember that the cameras are numbered from 0. For example, if you use only 3 cameras, they are numbered from 0 to 2, so only these **keys** will work. This applies to changing cameras and recording camera positions.*

*In the current version it is possible to use only 10 cameras at a time.*

## Other

In addition, the scripts contain a script called "Settings". This is a simple pause menu script. An example of a pause menu can be seen in the example scene. Thanks to this solution you do not have to change the value of each free camera separately, you can immediately change the values ​​for everyone in real time.


