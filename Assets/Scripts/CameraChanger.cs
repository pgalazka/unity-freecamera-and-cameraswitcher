﻿using UnityEngine;
using TMPro;

public class CameraChanger : MonoBehaviour
{
    [Header("List of available cameras")]
    public GameObject[] Cameras;

    [Header("Elements of the GUI")]
    public TextMeshProUGUI CameraIDText;
    public TextMeshProUGUI SetCameraText;
    public Animator m_Animator;

    private int CameraID = 0;

    //This is the list of keys for changing the selected camera
    readonly KeyCode[] keycodes = new KeyCode[] { KeyCode.Alpha0, KeyCode.Alpha1, KeyCode.Alpha2, KeyCode.Alpha3, KeyCode.Alpha4, KeyCode.Alpha5, KeyCode.Alpha6, KeyCode.Alpha7, KeyCode.Alpha8, KeyCode.Alpha9 };
    //This is a list of keys to save the position of the selected camera
    readonly KeyCode[] keycodesSet = new KeyCode[] { KeyCode.Keypad0, KeyCode.Keypad1, KeyCode.Keypad2, KeyCode.Keypad3, KeyCode.Keypad4, KeyCode.Keypad5, KeyCode.Keypad6, KeyCode.Keypad7, KeyCode.Keypad8, KeyCode.Keypad9};

    public void Update()
    {
        if(Cameras != null)
        {
            for(int i=0; i< Cameras.Length; i++) 
            {
               if(Input.GetKeyDown(keycodes[i])) 
               {
                    ChooseCamera(i);
               }
            }

            for (int i = 0; i < Cameras.Length; i++)
            {
                if (Input.GetKeyDown(keycodesSet[i]))
                {
                     SetCameraPosition(int.Parse(keycodesSet[i].ToString().Replace("Keypad", "")));
                }
            }

            if(Input.GetKeyDown(KeyCode.F))
            {
                ChooseCamera(0);
            }
        }
    }


    void ChooseCamera(int i)
    {
        CameraID = int.Parse(keycodes[i].ToString().Replace("Alpha", ""));
        if (CameraID >= 0 && CameraID < Cameras.Length)
        {
            foreach (GameObject Cam in Cameras)
            {
                Cam.SetActive(false);
            }
            Cameras[CameraID].SetActive(true);

            if(CameraIDText != null)
            { 
                    CameraIDText.text = "Camera " + CameraID.ToString();
            }
        }
    }
    void SetCameraPosition(int idCam)
    {
        if(idCam != CameraID) { 

            if(SetCameraText != null)
            {
                //Position of camera 1 has been changed
                SetCameraText.text = "Position of camera " + idCam + " has been changed";
                m_Animator.SetTrigger("CameraSet");
            }

            Cameras[idCam].transform.localPosition = Cameras[CameraID].transform.localPosition;
            Cameras[idCam].transform.localRotation = Cameras[CameraID].transform.localRotation;
        }
    }

}
