﻿using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    [Tooltip("Object representing the settings canvas")]
    public GameObject settings;
    [Tooltip("This is a scroll to change speed of camera in settings")]
    public Scrollbar cameraSpeedSlider;

    private bool isPause;
    private float valueOfCameraSpeed;

    private void Start()
    {
        isPause = false;
        
    }
    private void Update()
    {
        if (Input.GetKeyDown("escape"))
        {
            Pause();
        }

        if (isPause) 
        { 
            valueOfCameraSpeed = FindObjectOfType<FreeCamera>().GetCameraSpeedPercent();
            if(valueOfCameraSpeed != cameraSpeedSlider.value)
            {
                SetSettingsValue(valueOfCameraSpeed);
            }
        }
    }

   public void Pause()
    {
        if(isPause == false)
        {
            isPause = true;
            FindObjectOfType<CameraChanger>().enabled = false;
            settings.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            isPause = false;
            settings.SetActive(false);
            Time.timeScale = 1;
            FindObjectOfType<CameraChanger>().enabled = true;
        }
    }
    #region Settings
    public void SetSettingsValue(float value)
    {
        if (cameraSpeedSlider != null)
        {
            float valueOfCameraSpeed = FindObjectOfType<FreeCamera>().GetCameraSpeedPercent();
            cameraSpeedSlider.value = value;
        }
    }

    public void SetFreeCamerasSpeed(float SpeedVar)
    {
        foreach(FreeCamera freeCam in FindObjectsOfType<FreeCamera>())
        {
            freeCam.SetCameraSpeed(SpeedVar);
        }
    }

    public void SetMouseSpeed(float SpeedVar)
    {
        foreach (FreeCamera freeCam in FindObjectsOfType<FreeCamera>())
        {
            freeCam.SetMouseSpeed(SpeedVar);
        }
    }
    #endregion
    public void Quit()
    {
        Application.Quit();
    }
}
