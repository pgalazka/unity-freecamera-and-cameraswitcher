﻿using UnityEngine;
using TMPro;

public class FreeCamera : MonoBehaviour
{

    [Header("Free Camera Settings")]
    public float CameraSpeed;
    public float MouseSpeed;

    [Header("Free Camera Gui")]
    [Tooltip("Text representing the camera speed")]
    public TextMeshProUGUI CameraSpeedText;
    [Tooltip("Text representing the mouse speed")]
    public TextMeshProUGUI MouseSpeedText;

    private float _CameraSpeed;
    private float _MouseSpeed;
    private bool isLocked;
    private float RotationX, RotationY;

    private void Start()
    {
        _CameraSpeed = CameraSpeed;
        _MouseSpeed = MouseSpeed;
        isLocked = false;
    }

    private void LateUpdate()
    {
        #region GetInput
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        float UpDownInput = Input.GetAxis("UpDown");
        float MouseXInput = Input.GetAxis("Mouse X");
        float MouseYInput = Input.GetAxis("Mouse Y");
        float SpeedMovement = Input.GetAxis("Fire3") + 1;
        #endregion
        #region GetMouseRotation
        RotationX += MouseXInput * _MouseSpeed * Time.fixedDeltaTime;
        RotationY += MouseYInput * _MouseSpeed * Time.fixedDeltaTime;
        RotationY = Mathf.Clamp(RotationY, -90, 90);
        #endregion
        #region Move
        transform.position += transform.forward * verticalInput * (_CameraSpeed * SpeedMovement) * Time.fixedDeltaTime;
        transform.position += transform.right * horizontalInput * (_CameraSpeed * SpeedMovement) * Time.fixedDeltaTime;
        transform.position += transform.up * UpDownInput * _CameraSpeed * Time.fixedDeltaTime;
        #endregion
        #region SetMouseRotation
        if (Input.GetButton("Fire1")) 
        {
            if (isLocked == false)
                LockCursor();

            if(Input.mouseScrollDelta != Vector2.zero)
            {
                SetCameraSpeedWithScroll();
            }

            transform.localRotation = Quaternion.AngleAxis(RotationX, Vector3.up);
            transform.localRotation *= Quaternion.AngleAxis(RotationY, Vector3.left);
        }
        else
        {
            if (isLocked == true)
                UnlockCurosr();
        }
        #endregion
    }

    #region Set/Get Camera And Mouse Speed
    public void SetCameraSpeedWithScroll()
    {
        if (_CameraSpeed >= 0 && _CameraSpeed <= 100)
        {
            _CameraSpeed = _CameraSpeed + Input.mouseScrollDelta.y * 1;
            if (_CameraSpeed < 0) _CameraSpeed = 0;
            if (_CameraSpeed > 100) _CameraSpeed = 100;
        }
    }

    public void SetCameraSpeed(float SpeedVar)
    {
        SpeedVar = (float)System.Math.Round(SpeedVar, 2);
        _CameraSpeed = CameraSpeed * SpeedVar;
        if(CameraSpeedText != null)
            CameraSpeedText.text = SpeedVar.ToString();
    }

    public void SetMouseSpeed(float SpeedVar)
    {
        SpeedVar = (float)System.Math.Round(SpeedVar, 2);
        _MouseSpeed = MouseSpeed * SpeedVar;
        if (MouseSpeedText != null)
            MouseSpeedText.text = SpeedVar.ToString();
    }

    public float GetCameraSpeedPercent()
    {
        return (float)System.Math.Round(_CameraSpeed / CameraSpeed, 2);
    }
    #endregion
    #region Lock/Unlock Cursor
    void LockCursor()
    {
        isLocked = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    void UnlockCurosr()
    {
        isLocked = false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    #endregion

}
